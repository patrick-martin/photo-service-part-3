import fs from "fs";
//import { getImagesResponse, postImageResponse } from "../apiStubs";
//models represent code that persists our data objects
//this is where we want to get our image data to SEND BACK to controllers
//apiStubs moving out of 'controllers' and into 'models'

// () -> Promise<Array<URI: String>>
const find = () => {
  return fs.promises
    .readdir(process.env.RAZZLE_PUBLIC_DIR + "/images")
    .then(fileNames => {
      return fileNames.map(fileName => "/images/" + fileName);
    });

  //   return new Promise((resolve, reject) => {
  //     resolve(getImagesResponse.imageURIs);
  //   });
};

//fsPromises.readdir
//need to include (Buffer) as param
//buffer will be found in req.file.buffer
//remember, req.file will show the request
//fs.promises.writeFile
const create = buffer => {
  const timestamp = Date.now();
  return fs.promises
    .writeFile(process.env.RAZZLE_PUBLIC_DIR + "/images/" + timestamp, buffer)
    .then(() => {
      return "/images/" + timestamp;
    });
  //   return new Promise((resolve, reject) => {
  //     resolve(postImageResponse.imageURI);
  //   });
};

//this JS object represents our Image model
export default { find, create };
