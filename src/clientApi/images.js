import { getImagesResponse, postImageResponse } from "../apiStubs";
//this is just stub/dummy data..
//we will be talking to a third-party api and sending back actual data
//This is within clientApi folder, because client will be making these fetch requests
//when accessing "/api/images" or uploading an image

// () => Promise(getImagesResponse)
export const getImages = () => {
  console.log("getImages function fired!");
  return new Promise((resolve, reject) => {
    resolve(getImagesResponse);
  });
};

// () => Promise(postImageResponse) --> need to pass in formData somewhere
export const postImage = formData => {
  console.log("postImage function fired!");
  return new Promise((resolve, reject) => {
    resolve(postImageResponse);
  });
};
